<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codigo');
            $table->string('detalle');
            $table->integer('debe');
            $table->integer('haber');
            $table->string('preparado_por');
            $table->string('revisado_por');
            $table->string('aprobado_por');
            $table->string('nomb_empresa');
            $table->integer('ncde');
            $table->string('nombre');
            $table->integer('cantidad');
            $table->string('efectivo');
            $table->string('cheque');
            $table->string('banco');
            $table->date('fecha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diarios');
    }
}
