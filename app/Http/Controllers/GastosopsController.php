<?php

namespace Crud\Http\Controllers;
use Crud\Gastosop;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;

class GastosopsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gastosops = Gastosop::orderBy('id','ASC')->paginate(5);
        return view('admin.gastosops.index')->with('gastosops', $gastosops);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gastosops.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $gastosop=new Gastosop($request->all());
       $gastosop->save();
        Flash::warning("se ha insertado  " .$gastosop->cuenta. "de forma exitosa");
       return redirect()->route('gastosops.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gastosop=gastosop::find($id);
        return view('admin.gastosops.edit')->with('gastosop',$gastosop);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $gastosop=gastosop::find($id);
       $gastosop->cuenta=$request->cuenta;
       $gastosop->subcuenta=$request->subcuenta;
       $gastosop->save();
       Flash::warning('el usuario '. $gastosop->cuenta . ' ha sido editado con exito!');
       return redirect()->route('gastosops.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
