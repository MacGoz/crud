<?php
namespace Crud\Http\Controllers;

use Illuminate\Http\Request;
use Crud\User;
use Laracasts\Flash\Flash;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $users = User::orderBy('id','ASC')->paginate(5);
       return view('admin.users.index')->with('users',$users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user =new User($request->all());
        //encriptar password
        $user->password=bcrypt($request->password);
        $user->save();
        Flash::success("Se ha registrado " .$user->name. " de forma existosa");
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $user = User::find($id);

       return view('admin.users.edit')->with('user', $user);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $user=user::find($id);
       $user->name=$request->name;
       $user->email=$request->email;
       $user->type=$request->type;
       $user->save();
       Flash::warning('el usuario '. $user->name . ' ha sido editado con exito!');
       return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();

        Flash::error('El usuario ' . $user->name . ' ha sido borrado exitosamente!');
        return redirect()->route('users.index');
    }
}
