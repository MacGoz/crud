<?php

namespace Crud\Http\Controllers;

use Illuminate\Http\Request;
use  Crud\Diario;
use Laracasts\Flash\Flash;
class DiariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diarios = Diario::orderBy('id','ASC')->paginate(5);
        return view('admin.diarios.index')->with('diarios', $diarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.diarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $diario=new Diario($request->all());
       $diario->save();
        Flash::warning("se ha insertado  " .$diario->cuenta.  " de forma exitosa");
       return redirect()->route('diarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $diario=Diario::find($id);
        return view('admin.diarios.edit')->with('diario',$diario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $diario=Diario::find($id);
       $diario->cuenta=$request->cuenta;
       $diario->subcuenta=$request->subcuenta;
       $diario->save();
       Flash::warning('el usuario '. $diario->cuenta . ' ha sido editado con exito!');
       return redirect()->route('diarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
