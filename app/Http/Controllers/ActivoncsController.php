<?php

namespace Crud\Http\Controllers;
use Crud\Activonc;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;

class ActivoncsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activoncs= Activonc::OrderBy('id','ASC')->paginate(5);
        return view('admin.activoncs.index')->with('activoncs',$activoncs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.activoncs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $activonc=new Activonc($request->all());
       $activonc->save();
       Flash::success("se ha insertado  " .$activonc->cuentas. "de forma exitosa");
       return redirect()->route('activoncs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activonc=Activonc::find($id);
        return view('admin.activoncs.edit')->with('activonc',$activonc);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activonc=activonc::find($id);
       $activonc->cuenta=$request->cuenta;
       $activonc->subcuenta=$request->subcuenta;
       $activonc->save();
       Flash::warning('el usuario '. $activonc->cuenta . ' ha sido editado con exito!');
       return redirect()->route('activoncs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
