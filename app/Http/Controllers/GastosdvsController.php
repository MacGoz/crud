<?php

namespace Crud\Http\Controllers;
use Crud\Gastosdv;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;

class GastosdvsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $gastosdvs = Gastosdv::orderBy('id','ASC')->paginate(5);
        return view('admin.gastosdvs.index')->with('gastosdvs', $gastosdvs);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gastosdvs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gastosdv=new Gastosdv($request->all());
       $gastosdv->save();
        Flash::warning("se ha insertado  " .$gastosdv->cuenta. "de forma exitosa");
       return redirect()->route('gastosdvs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gastosdv=Gastosdv::find($id);
        return view('admin.gastosdvs.edit')->with('gastosdv',$gastosdv);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $gastosdv=Gastosdv::find($id);
       $gastosdv->cuenta=$request->cuenta;
       $gastosdv->subcuenta=$request->subcuenta;
       $gastosdv->save();
       Flash::warning('el usuario '. $gastosdv->cuenta . ' ha sido editado con exito!');
       return redirect()->route('gastosdvs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
