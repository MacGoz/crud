<?php

namespace Crud;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Diario extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo',
        'detalle',
        'debe',
        'haber',
        'preparado_por',
        'revisado_por',
        'aprobado_por',
        'nomb_empresa',
        'ncde',
        'nombre',
        'cantidad',
        'efectivo',
        'cheque',
        'banco',
        'fecha',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];
}
