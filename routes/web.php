<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('stargrid');
    return view('stargrid2');
    // return view('star_greed');
});

//prefijo de rutas
Route::group(['prefix' => 'admin'], function(){
Route::resource('users','UsersController');
Route::get('users/{id}/destroy',[
    'uses' =>'UsersController@destroy',
    'as' => 'users.destroy'
]);
// Route::resource('accounts','AccountsController');
Route::resource('activos','ActivosController');
Route::resource('activoncs','ActivoncsController');
Route::resource('gastosdvs','GastosdvsController');
Route::resource('gastosops','GastosopsController');
Route::resource('ingresos','IngresosController');
Route::resource('pasivoncs','PasivoncsController');
Route::resource('pasivopats','PasivopatsController');
Route::resource('diarios','DiariosController');

});
