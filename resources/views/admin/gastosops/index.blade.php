@extends('admin.template.main')
@section('title','Lista de Cuentas')

@section('content')
<a href="{{ route('gastosops.create') }}" class="btn btn-info">Registrar nueva cuenta</a>
<table class="table">
    <thead>
        <th>Id</th>
        <th>Cuenta</th>
        <th>Subcuenta</th>
        </thead>
    <tbody>
    @foreach($gastosops as $gastosop)
    <tr>
        <td>{{ $gastosop->id }}</td>
        <td>{{ $gastosop->cuenta }}</td>
        <td>{{ $gastosop->subcuenta }}</td>
        <td>
        @if($gastosop->type =="admin")
        <span class="label label-danger">{{ $gastosop->type }}</span>
        @else
        <span class="label label-primary">{{ $gastosop->type }}</span>
        @endif
        </td>
        <td><a href="{{ route('gastosops.edit', $gastosop->id)}}" class="btn btn-warning"><span class="glyphicon-wrench" aria-hidden="true"></span></a>
            <a href="{{ route('gastosops.destroy', $gastosop->id)}}" onclick="return confirm('¿Seguro deseas Eliminarlo')" class="btn btn-danger"><span class="glyphicon-remove-circle" aria-hidden="true"></span></a> </td>
    </tr>
@endforeach
    </tbody>
</table>
<div class="text-center">{!! $gastosops->render() !!}</div>

@endsection
