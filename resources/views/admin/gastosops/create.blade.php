@extends('admin.template.main')

@section('title', 'Crear cuenta')

@section('content')

    {!! Form::open(['route' => 'gastosops.store', 'method' => 'POST']) !!}
    <div class="form-group">
    {!! Form::label('cuenta', 'Cuenta') !!}
    {!! Form::text('cuenta',null, ['class' => 'form-control', 'placeholder' => 'Nombre Cuenta', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('subcuenta', 'Subcuenta') !!}
    {!! Form::text('subcuenta',null, ['class' => 'form-control', 'placeholder' => 'Nombre Subcuenta', 'required']) !!}
    </div>
    <!-- <div class="form-group">
    {!! Form::label('type', 'Tipo') !!}
    {!! Form::select('type', ['member' => 'Miembro', 'admin' => 'Administrador']) !!}
    </div> -->
    <div class="form-group">
	{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
	</div>
    {!! Form::close() !!}

@endsection
