@extends('admin.template.main')
@section('title','Lista de Cuentas')

@section('content')
<a href="{{ route('activoncs.create') }}" class="btn btn-info">Registrar nueva cuenta</a>
<table class="table">
    <thead>
        <th>Id</th>
        <th>Cuenta</th>
        <th>Subcuenta</th>
        </thead>
    <tbody>
    @foreach($activoncs as $activonc)
    <tr>
        <td>{{ $activonc->id }}</td>
        <td>{{ $activonc->cuenta }}</td>
        <td>{{ $activonc->subcuenta }}</td>
        <td>
        @if($activonc->type =="admin")
        <span class="label label-danger">{{ $activonc->type }}</span>
        @else
        <span class="label label-primary">{{ $activonc->type }}</span>
        @endif
        </td>
        <td><a href="{{ route('activoncs.edit', $activonc->id)}}" class="btn btn-warning"><span class="glyphicon-wrench" aria-hidden="true"></span></a>
            <a href="{{ route('activoncs.destroy', $activonc->id)}}" onclick="return confirm('¿Seguro deseas Eliminarlo')" class="btn btn-danger"><span class="glyphicon-remove-circle" aria-hidden="true"></span></a> </td>
    </tr>
@endforeach
    </tbody>
</table>
<div class="text-center">{!! $activoncs->render() !!}</div>

@endsection
