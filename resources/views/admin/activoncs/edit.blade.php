@extends('admin.template.main')

@section('title', 'Editar Cuenta'.$activonc->cuenta)

@section('content')

    {!! Form::open(['route' => ['activoncs.update',$activonc], 'method' => 'PUT']) !!}
    <div class="form-group">
    {!! Form::label('cuenta', 'Cuenta') !!}
    {!! Form::text('cuenta',$activonc->cuenta, ['class' => 'form-control', 'placeholder' => 'Nombre Cuenta', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('subcuenta', 'Subcuenta') !!}
    {!! Form::text('subcuenta',$activonc->subcuenta, ['class' => 'form-control', 'placeholder' => 'Nombre Subcuenta', 'required']) !!}
    </div>
    <!-- <div class="form-group">
    {!! Form::label('type', 'Tipo') !!}
    {!! Form::select('type', ['member' => 'Miembro', 'admin' => 'Administrador']) !!}
    </div> -->
    <div class="form-group">
	{!!Form::submit('Editar',['class'=>'btn btn-primary'])!!}
	</div>
    {!! Form::close() !!}

@endsection
