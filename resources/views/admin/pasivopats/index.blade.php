@extends('admin.template.main')
@section('title','Lista de Cuentas')

@section('content')
<a href="{{ route('pasivopats.create') }}" class="btn btn-info">Registrar nueva cuenta</a>
<table class="table">
    <thead>
        <th>Id</th>
        <th>Cuenta</th>
        <th>Subcuenta</th>
        </thead>
    <tbody>
    @foreach($pasivopats as $pasivopat)
    <tr>
        <td>{{ $pasivopat->id }}</td>
        <td>{{ $pasivopat->cuenta }}</td>
        <td>{{ $pasivopat->subcuenta }}</td>
        <td>
        @if($pasivopat->type =="admin")
        <span class="label label-danger">{{ $pasivopat->type }}</span>
        @else
        <span class="label label-primary">{{ $pasivopat->type }}</span>
        @endif
        </td>
        <td><a href="{{ route('pasivopats.edit', $pasivopat->id)}}" class="btn btn-warning"><span class="glyphicon-wrench" aria-hidden="true"></span></a>
            <a href="{{ route('pasivopats.destroy', $pasivopat->id)}}" onclick="return confirm('¿Seguro deseas Eliminarlo')" class="btn btn-danger"><span class="glyphicon-remove-circle" aria-hidden="true"></span></a> </td>
    </tr>
@endforeach
    </tbody>
</table>
<div class="text-center">{!! $pasivopats->render() !!}</div>

@endsection
