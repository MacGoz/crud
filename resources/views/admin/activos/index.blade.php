@extends('admin.template.main')
@section('title','Lista de Cuentas')

@section('content')
<a href="{{ route('activos.create') }}" class="btn btn-info">Registrar nueva cuenta</a>
<table class="table">
    <thead>
        <th>Id</th>
        <th>Cuenta</th>
        <th>Subcuenta</th>
        </thead>
    <tbody>
    @foreach($activos as $activo)
    <tr>
        <td>{{ $activo->id }}</td>
        <td>{{ $activo->cuenta }}</td>
        <td>{{ $activo->subcuenta }}</td>
        <td>
        @if($activo->type =="admin")
        <span class="label label-danger">{{ $activo->type }}</span>
        @else
        <span class="label label-primary">{{ $activo->type }}</span>
        @endif
        </td>
        <td><a href="{{ route('activos.edit', $activo->id)}}" class="btn btn-warning"><span class="glyphicon-wrench" aria-hidden="true"></span></a>
            <a href="{{ route('activos.destroy', $activo->id)}}" onclick="return confirm('¿Seguro deseas Eliminarlo')" class="btn btn-danger"><span class="glyphicon-remove-circle" aria-hidden="true"></span></a> </td>
    </tr>
@endforeach
    </tbody>
</table>
<div class="text-center">{!! $activos->render() !!}</div>

@endsection
