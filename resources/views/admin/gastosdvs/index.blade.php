@extends('admin.template.main')
@section('title','Lista de Cuentas')

@section('content')
<a href="{{ route('gastosdvs.create') }}" class="btn btn-info">Registrar nueva cuenta</a>
<table class="table">
    <thead>
        <th>Id</th>
        <th>Cuenta</th>
        <th>Subcuenta</th>
        </thead>
    <tbody>
    @foreach($gastosdvs as $gastosdv)
    <tr>
        <td>{{ $gastosdv->id }}</td>
        <td>{{ $gastosdv->cuenta }}</td>
        <td>{{ $gastosdv->subcuenta }}</td>
        <td>
        @if($gastosdv->type =="admin")
        <span class="label label-danger">{{ $gastosdv->type }}</span>
        @else
        <span class="label label-primary">{{ $gastosdv->type }}</span>
        @endif
        </td>
        <td><a href="{{ route('gastosdvs.edit', $gastosdv->id)}}" class="btn btn-warning"><span class="glyphicon-wrench" aria-hidden="true"></span></a>
            <a href="{{ route('gastosdvs.destroy', $gastosdv->id)}}" onclick="return confirm('¿Seguro deseas Eliminarlo')" class="btn btn-danger"><span class="glyphicon-remove-circle" aria-hidden="true"></span></a> </td>
    </tr>
@endforeach
    </tbody>
</table>
<div class="text-center">{!! $gastosdvs->render() !!}</div>

@endsection
