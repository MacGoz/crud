@extends('admin.template.main')
@section('title','Lista de Cuentas')

@section('content')
<a href="{{ route('pasivoncs.create') }}" class="btn btn-info">Registrar nueva cuenta</a>
<table class="table">
    <thead>
        <th>Id</th>
        <th>Cuenta</th>
        <th>Subcuenta</th>
        </thead>
    <tbody>
    @foreach($pasivoncs as $pasivonc)
    <tr>
        <td>{{ $pasivonc->id }}</td>
        <td>{{ $pasivonc->cuenta }}</td>
        <td>{{ $pasivonc->subcuenta }}</td>
        <td>
        @if($pasivonc->type =="admin")
        <span class="label label-danger">{{ $pasivonc->type }}</span>
        @else
        <span class="label label-primary">{{ $pasivonc->type }}</span>
        @endif
        </td>
        <td><a href="{{ route('pasivoncs.edit', $pasivonc->id)}}" class="btn btn-warning"><span class="glyphicon-wrench" aria-hidden="true"></span></a>
            <a href="{{ route('pasivoncs.destroy', $pasivonc->id)}}" onclick="return confirm('¿Seguro deseas Eliminarlo')" class="btn btn-danger"><span class="glyphicon-remove-circle" aria-hidden="true"></span></a> </td>
    </tr>
@endforeach
    </tbody>
</table>
<div class="text-center">{!! $pasivoncs->render() !!}</div>

@endsection
