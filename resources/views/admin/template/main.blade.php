<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title','Default')| Panel de Administracion</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">
</head>
<body>
<body class="admin-body">
    @include('admin.template.partials.nav')

    <section class="section-admin">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">@yield('title')</h3>
            </div>
            <div class="panel-body">
            @include('flash::message')
            @yield('content')
            </div>
        </div>
    </section>
<footer class="admin-footer">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="collapse navbar-collapse">
            <p class="navbar-text">DR &copy {{ date('Y')}}            </p>
            <p class="navbar-text navbar-right"><b>Sergraltec</b>  </p>
            </div>
        </div>
    </nav>
</footer>
<script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}  "></script>
<script src="{{ asset('plugins/jquery/js/jquery-3.3.1.js') }}  "></script>
</body>
</html>


