@extends('admin.template.main')
@section('title','Lista de Cuentas')

@section('content')
<a href="{{ route('accounts.create') }}" class="btn btn-info">Registrar nueva cuenta</a>
<table class="table">
    <thead>
        <th>Id_Cuenta</th>
        <th>Cuenta</th>
        <th>Subcuenta</th>
        </thead>
    <tbody>
    @foreach($users as$user)
    <tr>
        <td>{{ $user->id_cuenta }}</td>
        <td>{{ $user->cuenta }}</td>
        <td>{{ $user->subcuenta }}</td>
        <td>
        @if($user->type =="admin")
        <span class="label label-danger">{{ $user->type }}</span>
        @else
        <span class="label label-primary">{{ $user->type }}</span>
        @endif
        </td>
        <td><a href="{{ route('users.edit', $user->id)}}" class="btn btn-warning"><span class="glyphicon-wrench" aria-hidden="true"></span></a>
            <a href="{{ route('users.destroy', $user->id)}}" onclick="return confirm('¿Seguro deseas Eliminarlo')" class="btn btn-danger"><span class="glyphicon-remove-circle" aria-hidden="true"></span></a> </td>
    </tr>
@endforeach
    </tbody>
</table>
<div class="text-center">{!! $users->render() !!}</div>

@endsection
