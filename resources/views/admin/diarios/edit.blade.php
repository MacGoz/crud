@extends('admin.template.main')

@section('title', 'Editar Comprobante Diario Egreso'.$diario->codigo)

@section('content')

   {!! Form::open(['route' => ['diarios.update',$diario], 'method' => 'PUT']) !!}
    <div class="form-group">
    {!! Form::label('codigo', 'Codigo') !!}
    {!! Form::text('codigo',$codigo->codigo, ['class' => 'form-control', 'placeholder' => 'Nombre Cuenta', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('detalle', 'Detalle') !!}
    {!! Form::text('detalle',$detalle->detalle, ['class' => 'form-control', 'placeholder' => 'Codigo', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('debe', 'Debe') !!}
    {!! Form::text('debe',$debe->debe, ['class' => 'form-control', 'placeholder' => 'Detalle', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('haber', 'Haber') !!}
    {!! Form::text('haber',$haber->haber, ['class' => 'form-control', 'placeholder' => 'Debe', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('preparado_por', 'Preparado por') !!}
    {!! Form::text('preparado_por',$preparado_por->preparado_por, ['class' => 'form-control', 'placeholder' => 'Haber', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('revisado_por', 'Revisado por') !!}
    {!! Form::text('revisado_por',$revisado_por->revisado_por, ['class' => 'form-control', 'placeholder' => 'Preparado', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('aprobado_por', 'Aprobado por') !!}
    {!! Form::text('aprobado_por',$aprobado_por->aprobado_por, ['class' => 'form-control', 'placeholder' => 'Aprobado', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('nomb_empresa', 'Nombre empresa') !!}
    {!! Form::text('nomb_empresa',$nomb_empresa->nomb_empresa, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('ncde', 'N° CDE') !!}
    {!! Form::text('ncde',$ncde->ncde, ['class' => 'form-control', 'placeholder' => 'CDE', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('nombre', 'Pagado a') !!}
    {!! Form::text('nombre',$nombre->nombre, ['class' => 'form-control', 'placeholder' => 'Pagado a', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('cantidad', 'Suma de') !!}
    {!! Form::text('cantidad',$cantidad->cantidad, ['class' => 'form-control', 'placeholder' => 'Suma', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('efectivo', 'Efectivo') !!}
    {!! Form::text('efectivo',$efectivo->efectivo, ['class' => 'form-control', 'placeholder' => 'Efectivo', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('cheque', 'Cheque') !!}
    {!! Form::text('cheque',$cheque->cheque, ['class' => 'form-control', 'placeholder' => 'Cheque', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('banco', 'Banco') !!}
    {!! Form::text('banco',$banco->banco, ['class' => 'form-control', 'placeholder' => 'Banco', 'required']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('fecha', 'Fecha') !!}
    {!! Form::text('fecha',$fecha->fecha, ['class' => 'form-control', 'placeholder' => 'Fecha', 'required']) !!}
    </div>

    <div class="form-group">
    {!! Form::label('type', 'Tipo') !!}
    {!! Form::select('type', ['efectivo' => 'Efectivo', 'cheque' => 'Cheque', 'banco' => 'Banco']) !!}
    </div>
    <div class="form-group">
	{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
	</div>
    {!! Form::close() !!}

@endsection
