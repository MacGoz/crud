@extends('admin.template.main')
@section('title','Lista de Cuentas')

@section('content')
<a href="{{ route('diarios.create') }}" class="btn btn-info">Registrar nueva cuenta</a>
<table class="table">
    <thead>
        <th>Id</th>
        <th>codigo</th>
        <th>detalle</th>
        <th>debe</th>
        <th>haber</th>
        <th>preparado_por</th>
        <th>revisado_por</th>
        <th>aprobado_por</th>
        <th>nomb_empresa</th>
        <th>ncde</th>
        <th>nombre</th>
        <th>cantidad</th>
        <th>efectivo</th>
        <th>cheque</th>
        <th>banco</th>
        <th>fecha</th>
        </thead>
    <tbody>
    @foreach($diarios as $diario)
    <tr>
        <td>{{ $diario->id }}</td>
        <td>{{ $diario->codigo }}</td>
        <td>{{ $diario->detalle }}</td>
        <td>{{ $diario->debe }}</td>
        <td>{{ $diario->haber }}</td>
        <td>{{ $diario->preparado_por }}</td>
        <td>{{ $diario->revisado_por }}</td>
        <td>{{ $diario->aprobado_por }}</td>
        <td>{{ $diario->nomb_empresa }}</td>
        <td>{{ $diario->ncde }}</td>
        <td>{{ $diario->nombre }}</td>
        <td>{{ $diario->cantidad }}</td>
        <td>{{ $diario->efectivo }}</td>
        <td>{{ $diario->cheque }}</td>
        <td>{{ $diario->banco }}</td>
        <td>{{ $diario->fecha }}</td>
        <td>
        @if($diario->type =="admin")
        <span class="label label-danger">{{ $diario->type }}</span>
        @else
        <span class="label label-primary">{{ $diario->type }}</span>
        @endif
        </td>
        <td><a href="{{ route('diarios.edit', $diario->id)}}" class="btn btn-warning"><span class="glyphicon-wrench" aria-hidden="true"></span></a>
            <a href="{{ route('diarios.destroy', $diario->id)}}" onclick="return confirm('¿Seguro deseas Eliminarlo')" class="btn btn-danger"><span class="glyphicon-remove-circle" aria-hidden="true"></span></a> </td>
    </tr>
@endforeach
    </tbody>
</table>
<div class="text-center">{!! $diarios->render() !!}</div>

@endsection
