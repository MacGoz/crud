@extends('admin.template.main')

@section('title', 'Crear Comprobante Diario Egreso')

@section('content')

    {!! Form::open(['route' => 'diarios.store', 'method' => 'POST']) !!}

    <div class="form-row">
    <div class="form-group col-md-3">
    {!! Form::label('codigo', 'Codigo') !!}
    <div class="form-group">
    {!! Form::text('codigo',null, ['class' => 'form-control', 'placeholder' => 'Codigo', 'required']) !!}
    </div>
    </div>
    <div class="form-group col-md-3">
    {!! Form::label('detalle', 'Detalle') !!}
    <div class="form-group">
    {!! Form::text('detalle',null, ['class' => 'form-control', 'placeholder' => 'Detalle', 'required']) !!}
</div>
</div>

    <div class="form-group col-md-3">
    {!! Form::label('debe', 'Debe') !!}
    <div class="form-group">
    {!! Form::text('debe',null, ['class' => 'form-control', 'placeholder' => 'Debe', 'required']) !!}
</div>
</div>
    <div class="form-group col-md-3">
    {!! Form::label('haber', 'Haber') !!}
    <div class="form-group">
    {!! Form::text('haber',null, ['class' => 'form-control', 'placeholder' => 'Haber', 'required']) !!}
    </div>
    </div>

    </div>
    <div class="form-group col-md-3">
    {!! Form::label('preparado_por', 'Preparado por') !!}
    <div class="form-group">
    {!! Form::text('preparado_por',null, ['class' => 'form-control', 'placeholder' => 'Preparado', 'required']) !!}
    </div>
    </div>
    <div class="form-group col-md-3">
    {!! Form::label('revisado_por', 'Revisado por') !!}
    <div class="form-group">
    {!! Form::text('revisado_por',null, ['class' => 'form-control', 'placeholder' => 'Revisado', 'required']) !!}
    </div>
    </div>
    <div class="form-group col-md-3">
    {!! Form::label('aprobado_por', 'Aprobado por') !!}
    <div class="form-group">
    {!! Form::text('aprobado_por',null, ['class' => 'form-control', 'placeholder' => 'Aprobado', 'required']) !!}
    </div>
    </div>
    <div class="form-group col-md-3">
    {!! Form::label('nomb_empresa', 'Nombre empresa') !!}
    <div class="form-group">
    {!! Form::text('nomb_empresa',null, ['class' => 'form-control', 'placeholder' => 'empresa', 'required']) !!}
    </div>
    </div>
    <div class="form-group col-md-3">
    {!! Form::label('ncde', 'N° CDE') !!}
    <div class="form-group">
    {!! Form::text('ncde',null, ['class' => 'form-control', 'placeholder' => 'CDE', 'required']) !!}
    </div>
    </div>
    <div class="form-group col-md-3">
    {!! Form::label('nombre', 'Pagado a') !!}
    <div class="form-group">
    {!! Form::text('nombre',null, ['class' => 'form-control', 'placeholder' => 'Pagado a', 'required']) !!}
    </div>
    </div>
    <div class="form-group col-md-3">
    {!! Form::label('cantidad', 'Suma de') !!}
    <div class="form-group">
    {!! Form::text('cantidad',null, ['class' => 'form-control', 'placeholder' => 'Suma', 'required']) !!}
    </div>
    </div>
    <!-- <div class="form-group col-md-3">
    {!! Form::label('efectivo', 'Efectivo') !!}
    {!! Form::text('efectivo',null, ['class' => 'form-control', 'placeholder' => 'Efectivo', 'required']) !!}
    </div>
    <div class="form-group col-md-3">
    {!! Form::label('cheque', 'Cheque') !!}
    {!! Form::text('cheque',null, ['class' => 'form-control', 'placeholder' => 'Cheque', 'required']) !!}
    </div>
    <div class="form-group col-md-3">
    {!! Form::label('banco', 'Banco') !!}
    {!! Form::text('banco',null, ['class' => 'form-control', 'placeholder' => 'Banco', 'required']) !!}
    </div> -->
    <div class="form-group col-md-3">
    {!! Form::label('fecha', 'Fecha') !!}
    <div class="form-group">
    {!! Form::text('fecha',null, ['class' => 'form-control', 'placeholder' => 'Fecha', 'required']) !!}
    </div>
    </div>

     <div class="form-inline ">
    {!! Form::label('type', 'Tipo') !!}
    {!! Form::select('type', ['efectivo' => 'Efectivo', 'cheque' => 'Cheque', 'banco' => 'Banco']) !!}
    </div>
    <div class="form-group">
	{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
	</div>
    {!! Form::close() !!}

@endsection
