@extends('admin.template.main')
@section('title','Lista de Cuentas')

@section('content')
<a href="{{ route('ingresos.create') }}" class="btn btn-info">Registrar nueva cuenta</a>
<table class="table">
    <thead>
        <th>Id</th>
        <th>Cuenta</th>
        <th>Subcuenta</th>
        </thead>
    <tbody>
    @foreach($ingresos as $ingreso)
    <tr>
        <td>{{ $ingreso->id }}</td>
        <td>{{ $ingreso->cuenta }}</td>
        <td>{{ $ingreso->subcuenta }}</td>
        <td>
        @if($ingreso->type =="admin")
        <span class="label label-danger">{{ $ingreso->type }}</span>
        @else
        <span class="label label-primary">{{ $ingreso->type }}</span>
        @endif
        </td>
        <td><a href="{{ route('ingresos.edit', $ingreso->id)}}" class="btn btn-warning"><span class="glyphicon-wrench" aria-hidden="true"></span></a>
            <a href="{{ route('ingresos.destroy', $ingreso->id)}}" onclick="return confirm('¿Seguro deseas Eliminarlo')" class="btn btn-danger"><span class="glyphicon-remove-circle" aria-hidden="true"></span></a> </td>
    </tr>
@endforeach
    </tbody>
</table>
<div class="text-center">{!! $ingresos->render() !!}</div>

@endsection
